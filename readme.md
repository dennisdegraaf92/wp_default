﻿# Team Nijhuis Default WP KIT

 1. Maak een lokale development omgeving aan voor de nieuwe website
 2. Clone de bestanden uit deze repo naar de root van de development omgeving
 3. Open het commandprompt in de `wp-content\themes\default` directory
 4. Installeer NodeJS modules met de volgende opdrachten:
`npm install`
 5. Controleer gulp.js bestand in het theme
 6. Hervat installatie NodeJS modules met de volgende opdrachten:
 `npm install gulp-cli -g`
 `npm install gulp -D`
 7. Start gulp met de volgende opdracht
 `gulp`





