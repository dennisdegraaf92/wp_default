<?php

// ** MySQL settings Localhost ** //
define( 'DB_NAME', 'default' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', null );

define( 'DB_HOST', 'localhost' );

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_DISPLAY', true);
define( 'SCRIPT_DEBUG', true );
define( 'WP_LOCAL_DEV', true );
	
define( 'DOMAIN_CURRENT_SITE', 'default.test' );
define( 'WP_HOME', 'http://' . DOMAIN_CURRENT_SITE );

// ** Define license keys ** //
define( 'GF_LICENSE_KEY', '4d0b65c3aa64961f675bf2f53a6d294a' ); 
define( 'ACF_5_KEY', 'b3JkZXJfaWQ9ODk2OTN8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTA5LTE1IDA2OjU0OjIy' );
define( 'AKISMET_API_KEY', 'de7f3f9804eb' );
