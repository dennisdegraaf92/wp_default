<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
 
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
	include( dirname( __FILE__ ) . '/wp-config-local.php' );
} else {
	die( 'Environment not defined. Please add it in wp-config-local.php.' );
}

define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

define( 'WP_SITEURL', WP_HOME ); // site url	
define( 'WP_DEFAULT_THEME', 'default' ); // setting defaul theme

// ========================================================================
// Disable automatic updates
// @see http://codex.wordpress.org/Configuring_Automatic_Background_Updates
// ========================================================================
define( 'AUTOMATIC_UPDATER_DISABLED', true  );
define( 'WP_AUTO_UPDATE_CORE', false );

// ===============================================
// Some extra defines for performance and security
// ===============================================
define( 'WP_POST_REVISIONS', 3 );
define( 'AUTOSAVE_INTERVAL', 300 );
define( 'EMPTY_TRASH_DAYS', 365 );
define( 'CORE_UPGRADE_SKIP_NEW_BUNDLED', true );
define( 'DISALLOW_FILE_EDIT', true );
define( 'CONCATENATE_SCRIPTS', false );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2+>d?LaBre^}&}+1}3`CA7>KZ-TZ3gQYqI7#`$BSUPL|RYt-b:!R_p0>z:{rz;qO' );
define( 'SECURE_AUTH_KEY',  'XE3QD_#)f^7YX`UoG?;r4W(YAI{p.,KVXGxgT0Ul<t$DJ=t|Yg]6!8F-Z&TBJ3hE' );
define( 'LOGGED_IN_KEY',    'Oc+g@dwJSuu!HEs*j^.+iO`VZ+SBG9_}(#()pV;MsS8r%#Sp[VETr}.j7<qLXO}h' );
define( 'NONCE_KEY',        '0M??nq?zmwL-ic++V?nFjy[]|wpX7UhF?nuGf&`Ug+Hhx!G/o,eLXfifsq><!<$n' );
define( 'AUTH_SALT',        'E/+Ga s&6P 2dEI^n>`!mEg9[ff<sd8%&=r<k[+[&kB~{Z~Z?Pn+HfwE*Ep+_ysh' );
define( 'SECURE_AUTH_SALT', 'q(6Df|Z-8!VucidL6~ykp?#8?Wg#E>PCyGI,63bNV}3PT,HyWT] y&dHb,`)[pl]' );
define( 'LOGGED_IN_SALT',   'v-]%2TkP (t#4sF/=zybV#cgEK&Pls |OL(^^Q0[2,C5&WM+KV*}!,4f$/]mR&NR' );
define( 'NONCE_SALT',       'i|@iBX_Jg~eE;8MWT)>3<MCG@X@P3et:x:@w2u]1{~Z+#;7HtjsA>h[c||++Ny}/' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
