<?php
/*
	Plugin Name: Theme addons
	Description: Builds all Custom Post Types, Custom Taxonomies and Custom Meta Boxes for your website
	Author: Stef Dijkstra
	Version: 1.0
*/

define( 'CG_DIRNAME', dirname( plugin_basename( __FILE__ ) ) );
define( 'CG_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'CG_CLASS_PATH', CG_PLUGIN_PATH . '/classes/' );
define( 'CG_LANGUAGE_PATH', CG_DIRNAME . '/languages' ); ///needs to be relative to the plugins-base-dir
define( 'CG_PLUGIN_URI', WP_PLUGIN_URL . '/' .  CG_DIRNAME );

/**
 * Read the classes directory and include all php-files inside by 'require_once'.
 */
foreach ( glob( CG_CLASS_PATH . '*.php' ) as $file ) {
	require_once( $file );
}

//if it exists, create it!
if ( class_exists( 'Theme_Addons' ) ) {
	new Theme_Addons();
}