<?php
/*
 * All extra custom functions go inside this class, use this for image-sizes, ajax-callbacks et cetera
 * 
 * @author Stef Dijkstra
 */

if ( ! class_exists( 'Custom_Shortcodes' ) ) {

	class Custom_Shortcodes {

		public function __construct() {

			// Render site logo
			add_shortcode( 'logo', array( $this, 'render_logo_func' ) );

			// Render site logo diapositive
			add_shortcode( 'logo-diapositief', array( $this, 'render_logo_diapositive_func' ) );

			// Render social media share buttons 
			add_shortcode( 'social-media-share-buttons', array( $this, 'socialmedia_share_buttons_func' ) );

			// Render social media buttons 
			add_shortcode( 'social-media-icons', array( $this, 'socialmedia_buttons_func' ) );

			// Shortcode for rendering button
			add_shortcode( 'button', array( $this, 'button_func' ) );

			// Shortcode for rencering embed-video
			add_shortcode( 'embed_video', array( $this, 'embed_video_func' ) );

			// Render search form
			add_shortcode('search-form', array( $this, 'render_search_func' ));

			// Render stars
			add_shortcode('stars', array( $this, 'render_stars_func' ));

			// Render reviews
			add_shortcode('beoordelingen', array( $this, 'render_reviews_func' ));

		}

		// Render site logo
		public function render_logo_func() {
			$html = '';
			ob_start();
			   	get_template_part('template-parts/global/site-logo');
			    $html = ob_get_contents();
			ob_end_clean();
			return $html;
		}

		// Render site logo diapositive
		public function render_logo_diapositive_func() {
			$html = '';
			ob_start();
			   	get_template_part('template-parts/global/site-logo-diapositive');
			    $html = ob_get_contents();
			ob_end_clean();
			return $html;
		}


		// Render site logo
		public function render_search_func() {
			$html = '';
			ob_start();
			   	get_template_part('template-parts/global/search-form');
			    $html = ob_get_contents();
			ob_end_clean();
			return $html;
		}

		// Render social media share buttons 
		public function social_media_share_buttons_func() {

			$content = '';

			$url       = get_permalink( get_the_ID() );
			$title     = urlencode( get_the_title( get_the_ID() ) );
			$image_url = '';
			$image_id  = get_post_thumbnail_id( get_the_ID() );
			if ( isset( $image_id ) && ( ! empty( $image_id ) ) ) {
				$image = wp_get_attachment_image_src( $image_id, 'large' ); 
				if ( is_array( $image ) && ( ! empty( $image ) ) ) {
					$image_url = $image[0];
				}
			}

			//Twitter
			$twitter_url = 'https://twitter.com/intent/tweet';
			$twitter_url = add_query_arg( 'text', $title, $twitter_url );
			$twitter_url = add_query_arg( 'url', $url, $twitter_url );

			//Facebook
			$facebook_url = 'https://www.facebook.com/sharer/sharer.php';
			$facebook_url = add_query_arg( 'u', $url, $facebook_url );

			//Google
			$google_url = 'https://plus.google.com/share';
			$google_url = add_query_arg( 'url', $url, $google_url );

			//Whatsapp
			$whatsapp_url = 'whatsapp://send';
			$whatsapp_url = add_query_arg( 'text', $title . ' ' . $url, $whatsapp_url );

			//LinkedIn
			$linkedin_url = 'https://www.linkedin.com/shareArticle';
			$linkedin_url = add_query_arg( 'mini', 'true', $linkedin_url );
			$linkedin_url = add_query_arg( 'url', $url, $linkedin_url );
			$linkedin_url = add_query_arg( 'title', $title, $linkedin_url );

			//Pinterest
			$pinterest_url = 'https://pinterest.com/pin/create/button/';
			$pinterest_url = add_query_arg( 'url', $url, $pinterest_url );
			$pinterest_url = add_query_arg( 'description', $title, $pinterest_url );
			$pinterest_url = add_query_arg( 'media', $image_url, $pinterest_url );

			//Mail
			$mail_url = 'mailto:';
			$mail_url = add_query_arg( 'subject', $title, $mail_url );
			$mail_url = add_query_arg( 'body', urlencode(__( 'View this website:', 'theme-addons' ) . ' ' . $url )  , $mail_url );

			$content .= '	<ul class="sm-icons share">
								<li class="twitter">
									<a href="' . $twitter_url . '" target="_blank" title="' . esc_attr__( 'Share on Twitter', 'theme-addons' ) . '">
										<i class="fab fa-twitter"></i>
									</a>
								</li>
								<li class="facebook">
									<a href="' . $facebook_url . '" target="_blank" title="' . esc_attr__( 'Share on Facebook', 'theme-addons' ) . '">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<li class="linkedin">
									<a href="' . $linkedin_url . '" target="_blank" title="' . esc_attr__( 'Share on LinkedIn', 'theme-addons' ) . '">
										<i class="fab fa-linkedin-in"></i>
									</a>
								</li>
								<li class="email">
									<a href="' . $mail_url . '" title="' . esc_attr__( 'Share by e-mail', 'theme-addons' ) . '"><i class="fas fa-envelope"></i></a>
								</li>
								<li class="whatsapp">
									<a href="' . $whatsapp_url . '" title="' . esc_attr__( 'Share on Whatsapp', 'theme-addons' ) . '"><i class="fab fa-whatsapp"></i></a>
								</li>
							</ul>';

			return $content;
		}

		// Render social media buttons 
		public function socialmedia_buttons_func() {
			$html = '';
			if( have_rows('social_media', 'option') ) :
				$html .= '<ul class="sm-icons">';
				
				while( have_rows('social_media', 'option') ) : the_row(); 

					$html .= '	<li class="'. sanitize_title( get_sub_field('naam') ) .'">
									<a href="'. get_sub_field('url') .'" title="'. esc_attr( get_sub_field('naam') ) .'" target="_blank">
										'. get_sub_field('icoon') .'
									</a>
								</li>';

				endwhile;

				$html .= '</ul>';
			endif;

			return $html;		
		}
	
		// Shortcode for rendering button
		public function button_func( $atts ) {

			$color		= '';
			$size 		= '';
			$text		= '';
			$url		= '';
			$target		= '';
			$icon 		= '';
			$wrap_start	= '<p>';
			$wrap_end 	= '</p>';

			if( isset($atts['color']) && $atts['color'] !== '' )
				$color = $atts['color'];

			if( isset($atts['size']) && $atts['size'] !== '' )
				$size = $atts['size'];

			if( isset($atts['url']) && $atts['url'] !== '' )
				$url = $atts['url'];

			if( isset($atts['text']) && $atts['text'] !== '' )
				$text = $atts['text'];

			if( isset($atts['target']) && $atts['target'] !== '' )
				$target = ' target="' . $atts['target'] .'"';

			if( isset($atts['icon']) && $atts['icon'] !== '' )
				$icon = '<i class="fa '. $atts['icon'] . '"></i>';

			if( isset($atts['wrap']) && $atts['wrap'] == 'false' ) {
				$wrap_start	= '';
				$wrap_end 	= '';
			}

			$html = $wrap_start . '<a class="button '. $color . ' ' . $size . '" href="' . $url . '" ' . $target . '">' . $icon.$text . '</a>' . $wrap_end;

			return $html;
		}

		
		// Shortcode for rencering embed-video
		public function embed_video_func( $atts ) {

			$html = '';

			if( isset($atts['url']) && $atts['url'] !== '' ) {
				$url = $atts['url'];
				$html .= '<p class="responsive-iframe-wrapper"><iframe class="lazy" data-src="' . $url . '"></iframe></p>';
			} else {
				$html .= __('No url inserted', 'theme-addons'); 
			}

			return $html;
		}


		// Set integer to stars
		public function render_stars_func( $atts ) {

		    $html = '   <span class="star-container">';

		    $score  = $atts['score'];
		    $amount = round( $score );

		    switch( $amount ) :

		        case 1 :
		            $html .= '  <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>';
		            break;

		        case 2 :
		            $html .= '  <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>';
		            break;

		        case 3 :
		            $html .= '  <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>';
		            break;

		        case 4 :
		            $html .= '  <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star-o"></i></span>';
		            break;

		        case 5 :
		        default:
		            $html .= '  <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>
		                        <span class="star"><i class="fa fa-star"></i></span>';
		            break;
		            
		    endswitch;

		    $html .= ' </span>';

		    return $html;
		}

		public function render_reviews_func() {
			$html = '<p class="review-scores">';
			$html .= do_shortcode('[stars score="5"]');
			$html .= sprintf( __('%s %s reviews', 'theme-addons'), '9/10', 138);
			$html .= '</p>';
			return $html;
		}
	}


}