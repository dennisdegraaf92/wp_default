<?php
/*
 * All registrations for custom post types, have them made by a generator like on themergency.com and paste the functions in here, and have them run from: register_all_customposttypes()
 * 
 * @author Stef Dijkstra
 */
 
if ( ! class_exists( 'Custom_PostTypes' ) ) {
	
	class Custom_PostTypes  {
		
		public function __construct() {
			add_action( 'init', array( $this, 'register_all_customposttypes' ) );
		}

		public function register_all_customposttypes() {

			/*register_post_type( 'therapist',
				array(
					'labels' => array(
						'name'					=> __( 'Therapists', 'theme-addons' ),
						'singular_name'			=> __( 'Therapist', 'theme-addons' ),
						'menu_name'				=> __( 'Therapists', 'theme-addons' ),
						'name_admin_bar'		=> __( 'Therapists', 'theme-addons' ),
						'add_new'				=> __( 'Add new', 'theme-addons' ),
						'add_new_item'			=> __( 'Add new item', 'theme-addons' ),
						'new_item'				=> __( 'New item', 'theme-addons' ),
						'edit_item'				=> __( 'Edit item', 'theme-addons' ),
						'view_item'				=> __( 'View item', 'theme-addons' ),
						'all_items'				=> __( 'All items', 'theme-addons' ),
						'search_items'			=> __( 'Search items', 'theme-addons' ),
						'parent_item_colon'		=> __( 'Parent item', 'theme-addons' ),
						'not_found'				=> __( 'Not found', 'theme-addons' ),
						'not_found_in_trash'	=> __( 'Not found in trash', 'theme-addons' )						
					),
					'public'				=> false,
					'exclude_from_search'	=> true,
					'publicly_queryable'	=> true,
					'capability_type'		=> 'post',
					'show_ui'				=> true,
					'show_in_nav_menus'		=> true,
					'show_in_menu'			=> true,
					'show_in_admin_bar'		=> true,
					'rewrite'				=> array( 'slug'=> __('therapists', 'theme-addons' ), 'with_front' => false ),						
					'supports'				=> array( 'title', 'editor', 'thumbnail' ),
					'has_archive'			=> true,
					'menu_icon'				=> 'dashicons-businessperson',
					'query_var'				=> true,
					'menu_position'			=> 21
				)
			);

			if( function_exists('acf_add_options_page') ) {
			    
			    acf_add_options_sub_page(array(
			        'page_title'     	=> __('Therapist options', 'theme-addons'),
			        'menu_title'    	=> __('Therapist options', 'theme-addons'),
			        'menu_slug' 		=> 'therapist-options',
			        'parent_slug'    	=> 'edit.php?post_type=therapist'
			    ));

			}*/

		}

	}
}