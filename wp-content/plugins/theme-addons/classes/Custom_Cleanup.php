<?php
/*
 * Here we disable all the standard goodies from WP, we do not want...
 * 
 * @author Stef Dijkstra
 */
 
if ( ! class_exists( 'Custom_Cleanup' ) ) {
	
	class Custom_Cleanup {

		private static $instance;

		public function __construct() {

			//actions
			add_action( 'init', array( $this, 'disable_emojis' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'deregister_comments_scripts' ), 999 );
			add_action( 'template_redirect', array( $this, 'filter_comments_query' ), 9 );
			add_action( 'template_redirect', array( $this, 'filter_admin_bar' ) );
			add_action( 'admin_init', array( $this, 'filter_admin_bar' ) );
			add_action( 'admin_bar_menu', array( $this, 'remove_some_nodes_from_admin_top_bar_menu' ), 999 );
			add_action( 'admin_menu', array( $this, 'alter_admin_menu' ) );
			add_action( 'widgets_init', array( $this, 'unregister_default_widgets' ), 11 );
			add_action( 'wp_dashboard_setup', array( $this, 'filter_dashboard' ) );
			add_action( 'wp_loaded', array( $this, 'remove_post_type_comment_support' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'disable_superfish' ) );

			//remove actions
			remove_action( 'wp_head', 'feed_links_extra', 3 );
			remove_action( 'wp_head', 'wp_generator' );
			remove_action( 'wp_head', 'wp_resource_hints', 2 );
			remove_action( 'wp_head', 'feed_links', 2 );
			remove_action( 'wp_head', 'rest_output_link_wp_head' );
			remove_action( 'wp_head', 'rsd_link' );
			remove_action( 'wp_head', 'wlwmanifest_link' );

			//filters
			add_filter( 'wp_headers', array( $this, 'filter_wp_headers' ) );
			add_filter( 'allowed_block_types', array( $this, 'allowed_block_types' ), 10, 2 );

			//disable filters
			add_filter( 'pre_option_default_pingback_flag', '__return_zero' );
			add_filter( 'show_recent_comments_widget_style', '__return_false' );
			add_filter( 'comments_array', '__return_empty_array', 20, 2 );
			add_filter( 'comments_open', '__return_false', 20, 2 );
			add_filter( 'pings_open', '__return_false', 20, 2 );
			add_filter( 'get_comments_number', '__return_zero', 20, 2 );
			add_filter( 'xmlrpc_enabled', '__return_false' );
			add_filter( 'feed_links_show_comments_feed', '__return_false' );
		}

		/**
		 * Filter function used to remove the tinymce emoji plugin.
		 *
		 * @param array $plugins
		 *
		 * @return array Difference betwen the two arrays
		 */
		public function disable_emojis_tinymce( $plugins ) {
			if ( is_array( $plugins ) ) {
				return array_diff( $plugins, array( 'wpemoji' ) );
			} else {
				return array();
			}
		}

		/**
		 * Remove emoji CDN hostname from DNS prefetching hints.
		 *
		 * @param array $urls URLs to print for resource hints.
		 * @param string $relation_type The relation type the URLs are printed for.
		 *
		 * @return array Difference betwen the two arrays.
		 */
		public function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
			if ( 'dns-prefetch' == $relation_type ) {
				/** This filter is documented in wp-includes/formatting.php */
				$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

				$urls = array_diff( $urls, array( $emoji_svg_url ) );
			}

			return $urls;
		}

		//* Disable the superfish script
		public function disable_superfish() {
			wp_deregister_script( 'superfish' );
			wp_deregister_script( 'superfish-args' );
		}

		/**
		 * Disable the emoji's
		 */
		public function disable_emojis() {
			remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
			remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
			remove_action( 'wp_print_styles', 'print_emoji_styles' );
			remove_action( 'admin_print_styles', 'print_emoji_styles' );
			remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
			remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
			remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
			add_filter( 'tiny_mce_plugins', array( $this, 'disable_emojis_tinymce' ) );
			add_filter( 'wp_resource_hints', array( $this, 'disable_emojis_remove_dns_prefetch' ), 10, 2 );
		}

		public function allowed_block_types( $allowed_blocks, $post ) {

			return array(
				'core/paragraph',
				'core/image',
				'core/heading',
				'core/gallery',
				'core/list',
				'core/audio',
				'core/cover',
				'core/file',
				'core/video',
				'core/table',
				'core/verse',
				'core/code',
				'core/freeform',
				'core/html',
				'core/preformatted',
				'core/pullquote',
				'core/button',
				'core/text-columns',
				'core/media-text',
				'core/more',
				'core/nextpage',
				'core/separator',
				'core/spacer',
				'core/shortcode',
				'core/archives',
				'core/categories',
				'core/latest-comments',
				'core/latest-posts',
				'core/calendar',
				//'core/rss',
				//'core/search',
				'core/tag-cloud',
				'core/embed',
				'core-embed/twitter',
				'core-embed/youtube',
				'core-embed/facebook',
				'core-embed/instagram',
				'core-embed/wordpress',
				'core-embed/soundcloud',
				'core-embed/spotify',
				'core-embed/flickr',
				'core-embed/vimeo',
				'core-embed/animoto',
				'core-embed/cloudup',
				'core-embed/collegehumor',
				'core-embed/dailymotion',
				'core-embed/funnyordie',
				'core-embed/hulu',
				'core-embed/imgur',
				'core-embed/issuu',
				'core-embed/kickstarter',
				'core-embed/meetup-com',
				'core-embed/mixcloud',
				'core-embed/photobucket',
				'core-embed/polldaddy',
				'core-embed/reddit',
				'core-embed/reverbnation',
				'core-embed/screencast',
				'core-embed/scribd',
				'core-embed/slideshare',
				'core-embed/smugmug',
				'core-embed/speaker',
				'core-embed/ted',
				'core-embed/tumblr',
				'core-embed/videopress',
				'core-embed/wordpress-tv',
			);
		}

		public function remove_post_type_comment_support() {

			$disabled_post_types = get_post_types();

			if ( ! empty( $disabled_post_types ) ) {

				foreach ( $disabled_post_types as $type ) {

					// we need to know what native support was for later
					if ( post_type_supports( $type, 'comments' ) ) {

						//remove_post_type_support( $type, 'comments' );
						remove_post_type_support( $type, 'trackbacks' );
					}
				}
			}
		}

		public function filter_dashboard() {
			remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		}

		public function unregister_default_widgets() {

			// unregister all widgets
			//unregister_widget( 'WP_Widget_Media_Image' );
			unregister_widget( 'WP_Widget_Media_Audio' );
			unregister_widget( 'WP_Widget_Media_Video' );
			unregister_widget( 'WP_Widget_Media_Gallery' );
			//unregister_widget( 'WP_Widget_Custom_HTML' );
			unregister_widget( 'WP_Widget_Pages' );
			unregister_widget( 'WP_Widget_Calendar' );
			unregister_widget( 'WP_Widget_Archives' );
			unregister_widget( 'WP_Widget_Links' );
			unregister_widget( 'WP_Widget_Meta' );
			unregister_widget( 'WP_Widget_Search' );
			//unregister_widget( 'WP_Widget_Text' );
			unregister_widget( 'WP_Widget_Categories' );
			unregister_widget( 'WP_Widget_Recent_Posts' );
			unregister_widget( 'WP_Widget_Recent_Comments' );
			unregister_widget( 'WP_Widget_RSS' );
			unregister_widget( 'WP_Widget_Tag_Cloud' );
			//unregister_widget( 'WP_Nav_Menu_Widget' );
		}

		public function alter_admin_menu() {
			//remove_menu_page( 'index.php' );                  //Dashboard
			//remove_menu_page( 'jetpack' );                    //Jetpack*
			//remove_menu_page( 'edit.php' );                   //Posts
			//remove_menu_page( 'upload.php' );                 //Media
			//remove_menu_page( 'edit.php?post_type=page' );    //Pages
			remove_menu_page( 'edit-comments.php' );          //Comments
			//remove_menu_page( 'themes.php' );                 //Appearance
			//remove_menu_page( 'plugins.php' );                //Plugins
			//remove_menu_page( 'users.php' );                  //Users
			//remove_menu_page( 'tools.php' );                  //Tools
			//remove_menu_page( 'options-general.php' );        //Settings

			global $submenu;
			if ( isset( $submenu['themes.php'] ) ) {
				foreach ( $submenu['themes.php'] as $index => $menu_item ) {
					foreach ( $menu_item as $value ) {
						if ( strpos( $value, 'customize' ) !== false ) {
							unset( $submenu['themes.php'][ $index ] );
						}
					}
				}
			}

			remove_submenu_page( 'options-general.php', 'options-discussion.php' );
			//remove_submenu_page( 'options-general.php', 'options-media.php' );
		}

		/*
		 * Remove WordPress native customizer
		 */
		public function remove_some_nodes_from_admin_top_bar_menu( $wp_admin_bar ) {

			$wp_admin_bar->remove_menu( 'customize' );
		}

		/*
		 * Remove comment links from the admin bar.
		 */
		public function filter_admin_bar() {

			//if we even have an admin bar showing
			if ( is_admin_bar_showing() ) {

				//remove for single sites
				remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );

				//and remove for multisite
				if ( is_multisite() ) {
					add_action( 'admin_bar_menu', array( $this, 'remove_network_comment_links' ), 500 );
				}
			}
		}

		/*
		 * Remove comment links from the admin bar in a multisite network.
		 */
		public function remove_network_comment_links( $wp_admin_bar ) {
			if ( is_user_logged_in() ) {
				foreach ( (array) $wp_admin_bar->user->blogs as $blog ) {
					$wp_admin_bar->remove_menu( 'blog-' . $blog->userblog_id . '-c' );
				}
			} else {
				// We have no way to know whether the plugin is active on other sites, so only remove this one
				$wp_admin_bar->remove_menu( 'blog-' . get_current_blog_id() . '-c' );
			}
		}

		/*
		 * Issue a 403 for all comment feed requests.
		 */
		public function filter_comments_query() {

			//kill comment feeds
			if ( is_comment_feed() ) {
				wp_die( __( 'Comments are closed.', 'theme-addons' ), '', array( 'response' => 403 ) );
			}
		}

		/*
		 * Remove the X-Pingback HTTP header
		 */
		public function filter_wp_headers( $headers ) {
			unset( $headers['X-Pingback'] );

			return $headers;
		}

		/*
		 * Remove the X-Pingback HTTP header
		 */
		public function deregister_comments_scripts() {

			wp_deregister_script( 'comment-reply' );
		}

		/**
		 * Returns an instance of this class. An implementation of the singleton design pattern.
		 */
		public static function get_instance() {

			if ( null === self::$instance ) {
				self::$instance = new WP_Cleanup();
			}

			return self::$instance;
		}
	}
	
}