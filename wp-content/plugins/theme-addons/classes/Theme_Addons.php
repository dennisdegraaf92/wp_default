<?php
/*
 * Main Custom Goodies Class, initializes all needed subclasses and initializes the language file for translatations
 * 
 * @author Stef Dijkstra
 */

if ( ! class_exists( 'Theme_Addons' ) ) {
	
	class Theme_Addons  {
			
		public function __construct() {
			//load languages on init
    		add_action( 'init', array( $this, 'load_languagefile' ) );

    		//Build all the necessary custom taxonomies for all post types
    		new Custom_Cleanup();				

    		//Build all the necessary custom taxonomies for all post types
    		new Custom_Taxonomies();		

			//Build all the necessary custom post types
			new Custom_PostTypes();
			
			//Add some extra handy dandy functions or some function-functionality
			new Custom_Shortcodes();
			
			//Add some extra handy dandy functions or some function-functionality
			new Custom_Functions();
			
		}

		public function load_languagefile() {
			load_plugin_textdomain( 'theme-addons', false, CG_LANGUAGE_PATH );		
		}				
	}
}
