<?php
/*
 * All registrations for custom taxonomies, have them made by a generator like on themergency.com and paste the functions in here, and have them run from: register_all_customtaxonomies()
 * 
 * @author Stef Dijkstra
 */
 
if ( ! class_exists( 'Custom_Taxonomies' ) ) {
	
	class Custom_Taxonomies  {
		
		public function __construct() {
			add_action( 'init', array( $this, 'register_all_customtaxonomies' ) );	
		}
		
		public function register_all_customtaxonomies() {

			/*register_taxonomy('product_brand', array('product') , array (
				'labels' => array(
					'name' 				=> __( 'Brands', 'theme-addons' ),
					'singular_name' 	=> __( 'Brand', 'theme-addons' ),
					'menu_name'         => __( 'Brands', 'theme-addons' ),
				),
				'hierarchical'      => true,
				'show_ui'           => true,
				'show_admin_column' => true,
				'public'			=> true,
				'show_in_menu'		=> true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => __('brand', 'theme-addons') ),
			));*/
	
		}	

	}
	
}