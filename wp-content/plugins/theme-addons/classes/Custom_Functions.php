<?php
/*
 * All extra custom functions go inside this class, use this for image-sizes, ajax-callbacks et cetera
 * 
 * @author Stef Dijkstra
 */
 
if ( ! class_exists( 'Custom_Functions' ) ) {
	
	class Custom_Functions  {
		
		public function __construct() {

			// Add lazy loading functionality to images
			add_filter( 'wp_get_attachment_image_attributes', array( $this, 'add_lazyload_to_attachment_image' ), 10, 2 );

		}

		// Add lazy loading functionality to images
		public function add_lazyload_to_attachment_image( $attr, $attachment ) {

			$lazy = strstr( $attr['class'], 'lazy' );
			$slickLazy 	= strstr( $attr['class'], 'slick' );
			
			if ( $lazy ) {
				$attr['data-src'] = $attr['src'];
				$attr['src'] = '';

				if(	isset( $attr['srcset'] ) ) :
					$attr['data-srcset'] = $attr['srcset'];
					$attr['srcset'] = '';
				endif;
			}

			if ( $slickLazy ) {
				$attr['data-lazy'] = $attr['src'];
				$attr['src'] = '';

				if(	isset( $attr['srcset'] ) ) :
					$attr['data-srcset'] = $attr['srcset'];
					$attr['srcset'] = '';
				endif;
			}

			return $attr;
		}
	}
	
}