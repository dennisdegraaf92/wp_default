<?php
/**
	 * Plugin Name: Auto set license keys
	 * Description: Auto set license keys for several plugins after switching theme
	 * Version:     1.0
	 * Author:      Stef Dijkstra
 */

function auto_set_license_keys() {
  
  if ( ! get_option( 'acf_pro_license' ) && defined( 'ACF_5_KEY' ) ) {
    
    $save = array(
		'key'	=> ACF_5_KEY,
		'url'	=> home_url()
	);
	
	$save = maybe_serialize($save);
	$save = base64_encode($save);
      
    update_option( 'acf_pro_license', $save );
  }
}
add_action( 'activate_advanced-custom-fields-pro ', 'auto_set_license_keys' );