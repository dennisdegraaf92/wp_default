<?php
 /*
	Plugin Name: FLOTH Media Template Hints
	Plugin URI: 
	Description: Adds template hints to the admin bar. 
	Author: Floris P. Lof
	Version: 0.1
	Author URI: http://www.flothmedia.nl/
 */

class Template_Hints {
 	
	//the template path of the current path
	private $_templatepath = null;
	
	/**
	 * @function 	__construct
	 * @desc 		function for starting this class
	 * @return 		Void
	 */ 	
	public function __construct () {
		//add_action( 'admin_bar_init', array( $this, 'init' ) );
	}
	
	/**
	 * @function 	init
	 * @desc 		function for initializing the required functions at the right moment
	 * @return 		Void
	 */		
	public function init () {
		add_action( 'template_include', array( $this, 'define_template' ), 10, 1 );
		add_action( 'admin_bar_menu', array( $this, 'admin_bar_menu' ), 1000 );
	} 
	
	/**
	 * @function 	admin_bar_menu
	 * @desc 		function for adding the template-path as an element to the admin bar
	 * @return 		Void
	 */	
	public function admin_bar_menu () {
		global $wp_admin_bar;

		/* Add the main siteadmin menu item */
		$wp_admin_bar->add_menu( array(
			'id'     => 'template-hints',
			'parent' => 'top-secondary',
			'title'  => 'Template: ' . $this->_templatepath,
		) );		
	}
	
	/**
	 * @function 	define_template
	 * @desc 		Utility function for defining the name/location of the current rendered template file
	 * @param 		String $pFilePath
	 * @return 		String (the original $pFilePath)
	 */	
	public function define_template ( $pFilePath ) {
		$pathArr = explode( '/',  $pFilePath );
		$filename = array_pop( $pathArr );
		$themename = array_pop( $pathArr );
		$this->_templatepath = $themename .'/'. $filename;
		
		return $pFilePath;
	}

}
 
if( ! is_admin() ) { //only on the front-end
 	
 	new Template_Hints(); //start the class
 		
}