<?php
/*
	Plugin Name: FLOTH Media - Debugger
	Plugin URI: http://www.flothmedia.nl/
	Description: Adds some debugging functions and functionality while developing
	Author: Floris P. Lof
	Version: 1.1
	Author URI: http://www.flothmedia.nl/
*/

//Define a debug-state if not defined allready
if ( ! defined( 'WP_DEBUG') ) {
	if( isset( $_GET['debug'] ) && ( $_GET['debug'] == 'true' ) ) {
		define( 'WP_DEBUG', true );
	} else {
		define( 'WP_DEBUG', false );
	}
}

if( ! function_exists('print_r_pre') ) {
	/**
	 * @function 	print_r_pre
	 * @desc 		Utility function for making print_r prettier
	 * @param 		mixed $pData a mixed parameter to be printed on the screen
	 * @return 		void
	 */
	function print_r_pre( $pData ) {
		if( WP_DEBUG ) {
			print("<pre>"); 
			var_dump( $pData ); 
			print("</pre>"); 
		}
	}	
}

if( ! function_exists('writeln') ) {
	/**
	 * @function 	writeln
	 * @desc 		Utility function for printing a debugging line on a new line
	 * @param 		String $pLine to be printed on the screen
	 * @return 		void
	 */
	function writeln ( $pLine) {
		if( WP_DEBUG ) {
			print( $pLine . '<br />' );
		}
	}
}