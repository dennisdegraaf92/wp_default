<?php
/**
	 * Plugin Name: Disable plugins on local development and production
	 * Description: If the WP_LOCAL_DEV constant is true or false, disables plugins that you specify depending on local development or production.
	 * Version:     0.1
	 * License:     GPL version 2 or any later version
	 * Author:      Ezra Verheijen
	 * Original:    Mark Jaquith
	 * Plugin URI:  https://gist.github.com/markjaquith/1044546
	 * Author URI:  http://www.TriMM.nl/
 */

class Disable_Plugins_On_Local_Dev_And_Prod {
	static $instance;
	private $disabled = array();

	/**
	 * Sets up the options filter, and optionally handles an array of plugins to disable
	 * @param array $disables Optional array of plugin filenames to disable
	*/
	public function __construct( Array $disables = NULL) {
		// Handle what was passed in
		if ( is_array( $disables ) ) {
			foreach ( $disables as $disable )
				$this->disable( $disable );
		}

		// Add the filter
		add_filter( 'option_active_plugins', array( $this, 'do_disabling' ) );

		// Allow other plugins to access this instance
		self::$instance = $this;
	}

	/**
	 * Adds a filename to the list of plugins to disable
	 */
	public function disable( $file ) {
		$this->disabled[] = $file;
	}

	/**
	 * Hooks in to the option_active_plugins filter and does the disabling
	 * @param array $plugins WP-provided list of plugin filenames
	 * @return array The filtered array of plugin filenames
	 */
	public function do_disabling( $plugins ) {
		if ( count( $this->disabled ) ) {
			foreach ( (array) $this->disabled as $plugin ) {
				$key = array_search( $plugin, $plugins );
				if ( false !== $key )
					unset( $plugins[$key] );
			}
		}
		return $plugins;
	}
}

/* Begin customization */

// Local development and testing
if ( defined( 'WP_LOCAL_DEV' ) && WP_LOCAL_DEV ) {
	new Disable_Plugins_On_Local_Dev_And_Prod( array(

			'w3-total-cache/w3-total-cache.php',
			'wordfence/wordfence.php',
			'wp-rocket/wp-rocket.php'

	) );
	/*
	For programmatic disabling, you can initialize the object (e.g. as $_localdev) then do:
	$_localdev->disable( 'vaultpress.php' );
	*/
}

// Production and Acceptance
if ( defined( 'WP_LOCAL_DEV' ) && ! WP_LOCAL_DEV ) {
	new Disable_Plugins_On_Local_Dev_And_Prod( array(

			'debug-bar/debug-bar.php',
			'debug-bar-console/debug-bar-console.php',
			'genesis-visual-hook-guide/g-hooks.php',
			'floth-debugger/floth-debugger.php',
			'floth-template-hints/floth-template-hints.php',
	) );
}
