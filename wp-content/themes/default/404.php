<?php get_header(); ?>

<!-- Start #main -->
<main id="main" role="main">

	<?php get_template_part('template-parts/global/breadcrumbs'); ?>

	<?php get_template_part('template-parts/404/content'); ?>

</main>
<!-- End #main -->

<?php get_footer(); ?>