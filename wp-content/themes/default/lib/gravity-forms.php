<?php

/**
   * Default Gravity forms changes
*/

// Change loading icon from Gravity forms
add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );
function spinner_url( $image_src, $form ) {
    return get_template_directory_uri() . '/images/loading.gif';
}

// Change submit button on gravity forms
add_filter( 'gform_submit_button_1', 'form_submit_button', 10, 2);
function form_submit_button( $button, $form ){
    return '<button type="submit" class="button" id="gform_submit_button_' . $form["id"] . '">' . $form["button"]["text"] . '</button>';
}