<?php

/**
   * Register widgets
*/

add_action( 'widgets_init', 'tn_widgets_init' );
function tn_widgets_init() {

    register_sidebar( array(
        'name'          => __('Footer first block', 'teamnijhuis'),
        'id'            => 'footer_first_block',
        'before_widget' => '',
        'before_title'  => '',
        'after_title'   => ''
    ) );

    register_sidebar( array(
        'name'          => __('Footer second block', 'teamnijhuis'),
        'id'            => 'footer_second_block',
        'before_widget' => '',
        'before_title'  => '',
        'after_title'   => ''
    ) );

    register_sidebar( array(
        'name'          => __('Footer third block', 'teamnijhuis'),
        'id'            => 'footer_third_block',
        'before_widget' => '',
        'before_title'  => '',
        'after_title'   => ''
    ) );
}