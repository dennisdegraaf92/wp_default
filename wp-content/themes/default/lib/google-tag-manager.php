<?php

/**
   * Add Google tag manager
*/

// Google tag manager in <head>
add_action( 'wp_head', 'add_gtm_header', 99999 );
function add_gtm_header() {
    $gtmID = get_field('google_tag_manager_id', 'option');
    if ( isset( $gtmID ) && ( ! empty( $gtmID ) ) ) { ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $gtmID; ?>');</script>
    <!-- End Google Tag Manager -->
   <?php }       
}

// Google Tag manager script in body 
add_action( 'wp_body_open', 'add_gtm_body', 10 );
function add_gtm_body() { 
    $gtmID = get_field('google_tag_manager_id', 'option');
    if ( isset( $gtmID ) && ( ! empty( $gtmID ) ) ) {
    ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtmID; ?>"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } 
};
