<?php 

/**
   * Default WooCommerce changes
*/

// Disable default WooCommerce CSS
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// Replace the Woocommerce breadcrumbs by the Yoast breadcrumbs
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

// Action to get uncached cart
function load_woo_cart(){
	echo get_template_part( 'template-parts/woocommerce/parts/ajax-cart-products' );
	die();
}
add_action( 'wp_ajax_nopriv_load_woo_cart', 'load_woo_cart' );
add_action( 'wp_ajax_load_woo_cart', 'load_woo_cart' );


// Add flexbox classes to products in summary
add_filter( 'post_class', 'filter_product_post_class', 10, 3 );
function filter_product_post_class( $classes, $class, $product_id ){
   if( is_shop() || is_archive() ) 
      $classes[] = 'col-xs-12 col-md-6 col-lg-4';

   return $classes;
}


// Add Yoast breadcrumbs
add_action( 'woocommerce_before_main_content', function() {
  get_template_part('template-parts/global/breadcrumbs');
}, 10 );


// Wrap the WC content
add_action( 'woocommerce_before_main_content', function() {
   echo '   <div id="woocommerce-content" class="content-row">
               <div class="wrap">';
}, 10 );

add_action( 'woocommerce_after_main_content', function() {
   echo '      </div>
            </div>';
}, 10 );


// Wrap the product loop
add_action( 'woocommerce_before_shop_loop', function() {
   echo '     </div>
            </div>
            <div class="content-row">
              <div class="wrap">';
}, 30 );

add_action( 'woocommerce_after_shop_loop', function() {
   echo '     </div>
          </div>';
}, 30 );


// Wrap inside the product loop item
add_action( 'woocommerce_before_shop_loop_item', function() {
   echo '<article class="product-summary">';
}, 10 ); 

add_action( 'woocommerce_after_shop_loop_item', function() {
   echo '</article>';
}, 11 ); 


// Change the h2 loop to h3
if ( ! function_exists( 'woocommerce_template_loop_product_title' ) ) {
   function woocommerce_template_loop_product_title() {
      echo '<h3 class="woocommerce-loop-product__title">' . get_the_title() . '</h3>';
   }
}

// Wrap the featured images 
add_action( 'woocommerce_before_shop_loop_item_title', function(){
    echo '<p class="feat-img">';
}, 9 );
add_action( 'woocommerce_before_shop_loop_item_title', function(){
    echo '</p>';
}, 11 );

add_action( 'woocommerce_before_subcategory_title', function(){
    echo '<p class="feat-img">';
}, 9 );
add_action( 'woocommerce_before_subcategory_title', function(){
    echo '</p>';
}, 11 );


// Wrap the add to cart button
add_action( 'woocommerce_after_shop_loop_item', function() {
   echo '<p class="button-p">';
}, 5 );

add_action( 'woocommerce_after_shop_loop_item', function() {
   echo '</p>';
}, 10 );



/**
   * Cart lay-out
*/

// Auto update cart in Cart page
add_action( 'wp_footer', 'tn_cart_refresh_update_qty' );  
function tn_cart_refresh_update_qty() { 
   if (is_cart()) { 
      ?> 
      <script type="text/javascript"> 
         jQuery('div.woocommerce').on('focusout', 'input.qty', function(){ 
            jQuery("[name='update_cart']").trigger("click"); 
         }); 
      </script> 
      <?php 
   } 
}

// Wrap cart blocks in flex grid
add_action('woocommerce_before_cart', function() {
  echo '  <div class="row">
              <div class="col-xs-12 col-lg-8">
                <div class="block">';
}, 10 );

add_action('woocommerce_after_cart_table', function() {
  echo '      </div>
            </div>
            <div class="col-xs-12 col-lg-4 ">
              <div class="block">';
}, 10);

add_action('woocommerce_afer_cart', function() {
  echo '      </div>
          </div>';
}, 10 );

// Wrap checkout blocks in flex grid
add_action( 'woocommerce_checkout_before_customer_details', function() {
  echo '  <div class="row">
              <div class="col-xs-12 col-lg-8">
                <div class="block">';
}, 0);

add_action( 'woocommerce_checkout_after_customer_details', function() {
  echo '        </div>
              </div>
              <div class="col-xs-12 col-lg-4 ">
                <div class="block">';
}, 10 );

add_action( 'woocommerce_checkout_after_order_review', function() {
  echo '        </div>
              </div>
            </div>';
}, 10 );

add_action( 'woocommerce_review_order_before_payment', function(){
    echo '<p class="h3">' . __('Payment method', 'teamnijhuis') . '</p>'; 
}, 10 );



/**
   * My account
*/


// Wrap account blocks in flex grid
add_action( 'woocommerce_before_account_navigation', function(){
  echo '  <div class="row">
            <div class="col-xs-12 col-md-4 ">
                <div class="block">';
}, 0 );

add_action( 'woocommerce_after_account_navigation', function(){
  echo '       </div>
            </div>
            <div class="col-xs-12 col-md-8 ">';
}, 50 );

add_action( 'woocommerce_account_dashboard', function(){
  echo '    </div>
          </div>';
}, 50 );


