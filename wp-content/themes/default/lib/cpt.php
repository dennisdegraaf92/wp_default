<?php

/**
   * CPT customs
*/

// Disable single-reviews
//add_action( 'template_redirect', 'tn_redirect_post' );
function tn_redirect_post() {
    $queried_post_type = get_query_var('post_type');
    if ( is_single() && 'location' ==  $queried_post_type ) {
        wp_redirect( get_post_type_archive_link('location'), 301 );
    exit;
    }
}

// Set amount of posts for review acrhive
//add_action( 'pre_get_posts', 'set_posts_per_page_for_reviews_cpt' );
function set_posts_per_page_for_reviews_cpt( $query ) {
    if ( !is_admin() && $query->is_main_query() ) {

        if( is_post_type_archive( 'product' ) || is_post_type_archive( 'product' ) ) { 
           $query->set( 'post_parent', '0' );
        }
    }
}
