<?php 

// Add Admin CSS
add_action( 'after_setup_theme', 'tn_theme_setup' );
if ( ! function_exists( 'tn_theme_setup' ) ) {
    function tn_theme_setup() {
        add_action( 'init', 'tn_mce_buttons' );
    }
}

// Add custom tinymce buttons
if ( ! function_exists( 'tn_mce_buttons' ) ) {
    function tn_mce_buttons() {
        if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
            return;
        }

        if ( get_user_option( 'rich_editing' ) !== 'true' ) {
            return;
        }

        add_filter( 'mce_external_plugins', 'tn_add_buttons' );
        add_filter( 'mce_buttons', 'tn_register_buttons' );
    }
}

// Register TinyMCE Buttons for shortcodes
if ( ! function_exists( 'tn_add_buttons' ) ) {
    function tn_add_buttons( $plugin_array ) {
        $plugin_array['addbutton'] = get_template_directory_uri().'/assets/js/tinymce_buttons.js';
        return $plugin_array;
    }
}

if ( ! function_exists( 'tn_register_buttons' ) ) {
    function tn_register_buttons( $buttons ) {
        array_push( $buttons, 'addbutton' );
        return $buttons;
    }
}


// TinyMCE change button labels
add_action ( 'after_wp_tiny_mce', 'tn_tinymce_extra_vars' );
if ( !function_exists( 'tn_tinymce_extra_vars' ) ) {
  function tn_tinymce_extra_vars() { ?>
    <script type="text/javascript">
      var tinyMCE_object = <?php echo json_encode(
        array(
          'button_name' => esc_html__('CTA button', 'teamnijhuis'),
          'button_title' => esc_html__('Add button', 'teamnijhuis')
        )
        );
      ?>;
    </script><?php
  }
}