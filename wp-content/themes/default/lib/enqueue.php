<?php

/**
   * Enqueue scripts
*/

// Adding CSS + JS files
add_action( 'wp_enqueue_scripts', 'tn_enqueue_scripts_styles' );
function tn_enqueue_scripts_styles() {
    // Css scripts
    wp_enqueue_style( 'tn-webfonts', '', false, false );
    $version = filemtime(  trailingslashit( get_template_directory() ) . 'style.min.css' );
    wp_enqueue_style( 'tn-style-css', trailingslashit( get_template_directory_uri() ) . 'style.min.css', false, $version );

    // Js scripts
    wp_enqueue_script( 'jquery' );
    $version = filemtime(  trailingslashit( get_template_directory() ) . 'js/script.min.js' );
    wp_enqueue_script( 'tn-scripts',  get_template_directory_uri() . '/js/script.min.js', array( 'jquery' ), $version, true );

    // Dequeue styles
    wp_dequeue_style( 'font-awesome-css' ); // Default Font Awesome by plug-in, no duplicates
}

// Add fontawesome to footer
add_action('wp_footer', 'add_font_awesome_css', 30);
function add_font_awesome_css() {
    ?>
    <link rel="preload" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="styles.css"></noscript>
<?php
}

// Disable jQuery migrate on front
function dequeue_jquery_migrate( $scripts ) {
    if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) 
        $scripts->registered['jquery']->deps = array_diff( $scripts->registered['jquery']->deps, [ 'jquery-migrate' ] );
}
add_action( 'wp_default_scripts', 'dequeue_jquery_migrate' );


// Register nav menu theme-locations
register_nav_menus( array(
    'main-navigation'       => __('Main navigation', 'teamnijhuis'),
    'top-navigation'       => __('Top navigation', 'teamnijhuis'),
));


// Loads the main.js, registered as 'my-handle', javascript file script asynchronously
//add_filter( 'script_loader_tag', 'load_js_asynchronously', 10, 3 );
function load_js_asynchronously( $tag, $handle, $src ) {
    if ( !is_admin() ) {
        $scripts = array('tn-main');
        if ( !in_array($handle, $scripts) ) 
            return $tag;    

        return '<script src="' . $src . '" async></script>';
    } else {
        return $tag;
    }
}

//Loads all stylesheets asynchronously
//add_filter( 'style_loader_tag', 'load_css_asynchronously', 10, 3 );
function load_css_asynchronously( $tag, $handle, $src ) {
    if ( !is_admin() ) {
        $new_tag = str_replace( "rel='stylesheet'", 'rel="preload" as="style" onload="this.onload=null;this.rel=\'stylesheet\'"', $tag );
        $new_tag .= '<noscript>' . $tag . '</noscript>';
        return $new_tag;
    } else {
        return $tag;
    }
}