<?php

/**
   * Add image sizes and customs
*/

// Add extra image sizes
add_image_size( 'small', 300, 300, true );
add_image_size( 'desktop', 1920, 1080, false );
add_filter( 'image_size_names_choose', 'tn_custom_sizes' ); 
function tn_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'small'         => __( 'Small', 'teamnijhuis' ),
        'desktop'       => __( 'Desktop', 'teamnijhuis' ),
    ) );
}