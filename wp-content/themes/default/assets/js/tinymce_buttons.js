(function() {
    tinymce.PluginManager.add('addbutton', function( editor, url ) {
        editor.addButton( 'addbutton', {
            text: tinyMCE_object.button_name,
            icon: false,
            onclick: function() {
                editor.windowManager.open( {
                    title: tinyMCE_object.button_title,
                    body: [
                        {
                            type: 'textbox',
                            name: 'button_text',
                            label: 'Tekst in button',
                        },
                        {
                            type: 'textbox',
                            name: 'button_url',
                            label: 'URL',
                        },
                        {
                            type   : 'listbox',
                            name   : 'button_color',
                            label  : 'Kies een kleur',
                            values : [
                                { text: 'Geel', value: 'yellow' },
                                { text: 'Blauw ghost', value: 'blue-ghost' },
                                { text: 'Wit', value: 'white' },
                                { text: 'Transparent', value: 'transparent' }
                            ],
                            value : 'yellow' // Sets the default
                        },
                        {
                            type   : 'listbox',
                            name   : 'button_target',
                            label  : 'Soort link',
                            values : [
                                { text: 'Intern', value: '_self' },
                                { text: 'Extern', value: '_blank' },
                            ],
                            value : 'violet' // Sets the default
                        }
                    ],
                    onsubmit: function( e ) {
                        editor.insertContent( '[button text="' + e.data.button_text + '" color="' + e.data.button_color + '" url="'+ e.data.button_url +'" target="' + e.data.button_target + '"  ]');
                    }
                });
            },
        });
    });

})();

jQuery(document).ready(function($){
    $(document).on('click', '.mce-my_upload_button', upload_image_tinymce);

    function upload_image_tinymce(e) {
        e.preventDefault();
        var $input_field = $('.mce-my_input_image');
        var custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Add Image',
            button: {
                text: 'Add Image'
            },
            multiple: false
        });
        custom_uploader.on('select', function() {
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            $input_field.val(attachment.url);
        });
        custom_uploader.open();
    }
});