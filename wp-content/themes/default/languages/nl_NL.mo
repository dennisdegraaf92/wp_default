��    #      4  /   L        
   	       8   $  
   ]  
   h     s  %   x     �     �     �     �  
   �     �     �            1         R     g     v  	   �     �     �     �  
   �     �     �     �  l   �     X     g     {     �     �  �  �     d     s  8   �     �     �     �  %   �     	     	     (	     ;	  
   M	     X	     j	     y	     ~	  E   �	     �	     �	     
     
     
  
   1
     <
     L
     U
     d
     j
  w   w
     �
     �
          #     *                   #                                                                      
                  	   !                                        "       Add button Back to summary Below you will find the results for your search term %s. By %s | %s CTA button Cart Custom theme created by Team Nijhuis. Desktop Footer first block Footer second block Footer third block Front page Hide navigation Main navigation Menu No items found. No matching results were found. Please try again. Page not found (404) Payment method Picabia WP theme Read more Recent articles Search for: Searched for %s Share now! Show navigation Small Team Nijhuis The page you requested is not available at this time. Click <a href="%s">here</a> to return to the homepage. Unfortunately! https://picabia.nl/ https://teamnijhuis.com teamnijhuisSearch teamnijhuisSearch &hellip; Project-Id-Version: Picabia WP theme
PO-Revision-Date: 2020-05-19 10:08+0200
Last-Translator: 
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Knop toevoegen Terug naar het overzicht Hieronder vindt u de resultaten voor uw zoekopdracht %s. Door %s | %s CTA knop Winkelwagen Custom theme created by Team Nijhuis. Desktop Footer eerste blok Footer tweede blok Footer derde blok Voorpagina Verberg navigatie Hoofdnavigatie Menu Geen items gevonden. Er zijn geen overeenkomende resultaten gevonden. Probeer het opnieuw. Pagina niet gevonden (404) Betaalmethode Picabia WP theme Lees verder Recente artikelen Zoek naar: Gezocht naar %s Deel nu! Toon navigatie Klein Team Nijhuis De opgevraagde pagina is niet beschikbaar op dit moment. Klik <a href="%s">hier</a> om terug te keren naar de homepage. Helaas! https://picabia.nl/ https://teamnijhuis.com Zoeken Zoeken &hellip; 