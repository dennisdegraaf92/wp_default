<?php

// Add menu support to theme
if( function_exists( 'add_theme_support' ) ) :

    add_theme_support( 'nav-menus' );
    add_theme_support( 'menus' );
    add_theme_support( 'post-thumbnails' );
    //add_theme_support( 'woocommerce' );
    //add_theme_support( 'wc-product-gallery-zoom' );
    //add_theme_support( 'wc-product-gallery-lightbox' );
    //add_theme_support( 'wc-product-gallery-slider' );
    load_theme_textdomain( 'teamnijhuis', get_template_directory() . '/languages' );

endif;

// Enable options page for website
if( function_exists('acf_add_options_page') )    
    acf_add_options_page();    


// Enqueue scipts
require_once( get_template_directory() . '/lib/enqueue.php' );

// Register widgets
require_once( get_template_directory() . '/lib/widgets.php' );

// Add images sizes
require_once( get_template_directory() . '/lib/images.php' );

// Custom buttons is WYSWYG-editor
require_once( get_template_directory() . '/lib/wysiwyg.php' );

// Custom post types customs
require_once( get_template_directory() . '/lib/cpt.php' );

// Enqueue scipts
require_once( get_template_directory() . '/lib/google-tag-manager.php' );

// Gravityforms customs
if ( class_exists( 'GFForms' ) ) 
    require_once( get_template_directory() . '/lib/gravity-forms.php' );

// Check if WooCommerce is active and add default changes
if ( class_exists( 'WooCommerce' ) ) 
    require_once( get_template_directory() . '/lib/woocommerce/woocommerce.php' );
