/* Needed gulp config */
const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
var postcss = require('gulp-postcss');
var reporter = require('postcss-reporter');
var syntax_scss = require('postcss-scss');
var stylelint = require('stylelint');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const plumber = require('gulp-plumber');
const browserSync = require('browser-sync');
const reload = browserSync.reload;

const distWpTheme = '.';
const paths = {
	css_src: distWpTheme + '/sass/*.scss',
	all_css_src: distWpTheme + '/sass/**/*.scss',
	css_dist: distWpTheme,

	js_src: distWpTheme + '/js/script.js',
	all_js_src: distWpTheme + '/js/concat/*.js',
	js_dist: distWpTheme + '/js',

	img_src: distWpTheme + '/images/**/*',
	img_dist: distWpTheme + '/images',

	all_php_src: [ 
		distWpTheme + '/*.php', 
		distWpTheme + '/**/*.php'
	]
};

/* Scripts task */
gulp.task('scripts', () => {
	return gulp.src(paths.all_js_src)
	    .pipe(concat('script.js'))
	    .pipe(gulp.dest(paths.js_dist))
	    .pipe(rename({suffix: '.min'}))
	    .pipe(uglify().on('error', function (e) {
	    	this.emit('end');
	    }))
	    .pipe(gulp.dest(paths.js_dist));
});

/* Sass task */
gulp.task('sass', () => {
	return gulp.src(paths.css_src)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', handleErrors))
		.pipe(autoprefixer('last 2 version'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.css_dist))
		.pipe(rename({suffix: '.min'}))
		.pipe(cssnano({
			zindex: false
		}))
		.pipe(gulp.dest(paths.css_dist))
		.pipe(reload({stream: true}))
});

gulp.task("scss-lint", () => {
	var processors = [
		stylelint(),
		reporter({
			clearMessages: true,
			throwError: false
		})
	];

	return gulp.src(paths.css_src)
		.pipe(postcss(processors, {syntax: syntax_scss}));
});

/* Minify images */
gulp.task('imagemin', function () {
	return gulp.src(paths.img_src)
		.pipe(gulp.dest(paths.img_dist));
});

/* Prepare Browser-sync for localhost */
gulp.task('serve', function () {
	browserSync.init([paths.css_src, paths.js_src, paths.img_src], {
		open: false,
		injectChanges: true,
		proxy: 'http://default.test',
		notify: false,
		https: false,
		ghostMode: {
			clicks: true,
			scroll: true,
			links: true,
			forms: true
		},
		reloadDelay: 300,
		watchOptions: {
			debounceDelay: 500
		}
	});

	gulp.watch(paths.all_css_src, gulp.series(['sass', 'scss-lint'])).on('change', browserSync.stream);
	gulp.watch(paths.all_js_src, gulp.series(['scripts'])).on('change', browserSync.reload);
	gulp.watch(paths.all_php_src).on('change', browserSync.reload);
});

gulp.task('default', gulp.series(['sass', 'scripts', 'imagemin', 'serve']));

/* Function to handle the errors */
function handleErrors() {
	const args = Array.prototype.slice.call(arguments);

	// Send error to notification center with gulp-notify
	notify.onError({
		title: 'Compile Error',
		message: '<%= error.message %>',
		sound: 'Sosumi'
	}).apply(this, args);

	this.emit('end');
}
// node ./node_modules/gulp/bin/gulp.js