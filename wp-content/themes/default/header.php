<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>	
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
<?php do_action('wp_body_open'); ?>

<!-- Start #main-header -->
<header id="main-header">

	<?php get_template_part('template-parts/header/nav-bar'); ?>

</header>
<!-- End #main-header -->