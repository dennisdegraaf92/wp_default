<?php 
	$bg_color = get_sub_field('achtergrondkleur');
	$white_text_array = array('blue-bg', 'light-blue-bg', 'orange-bg');
?>

<!-- Start .default-text -->
<div class="content-row default-text <?php echo $bg_color; ?>">
	
	<div class="wrap<?php echo ( in_array($bg_color, $white_text_array )? ' white-text' : '' ); ?>">
		
		<?php the_sub_field('tekst'); ?>

	</div>

</div>
<!-- End .default-text -->