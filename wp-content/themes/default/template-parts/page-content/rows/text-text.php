<?php 
	$bg_color = get_sub_field('achtergrondkleur');
	$white_text_array = array('blue-bg', 'light-blue-bg', 'orange-bg');
?>

<!-- Start .text-text -->
<div class="content-row text-text <?php echo $bg_color; ?>">
	
	<div class="wrap<?php echo ( in_array($bg_color, $white_text_array )? ' white-text' : '' ); ?>">

		<div class="row">

			<div class="col-xs-12 col-md-6">

				<?php the_sub_field('tekst_links'); ?>

			</div>

			<div class="col-xs-12 col-md-6">

				<?php the_sub_field('tekst_rechts'); ?>

			</div>

		</div>

	</div>

</div>
<!-- End .text-text -->