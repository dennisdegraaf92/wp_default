<!-- Start #page-content -->
<?php if( have_rows('pagina_content') ) : ?>

	<div id="page-content">

		<?php
		    while ( have_rows( 'pagina_content' ) ) : the_row();

		    	switch( get_row_layout() ) {

		    		case 'text' :
		    			get_template_part('template-parts/page-content/rows/text');
		    		break;

		    		case 'text_text' :
		    			get_template_part('template-parts/page-content/rows/text-text');
		    		break;
		    	}

		    endwhile;
		?>

	</div>

<?php endif; ?>
<!-- End #page-content -->