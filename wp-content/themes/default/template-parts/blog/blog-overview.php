<?php $blogpage = get_post( get_option('page_for_posts') ); ?>

<!-- Start #blog-page -->
<div id="blog-page" class="content-row">

	<div class="wrap">

		<div class="text-wrap">

			<?php 
				if( is_category() ) {
					$queried_object = get_queried_object();
					echo '<h1>'. $queried_object->name .'</h1>';
					echo apply_filters( 'the_content', $queried_object->description );
				} else {
					echo '<h1>' . $blogpage->post_title . '</h1>';
				}
			?>			

		</div>

	</div>

	<?php 
		if( have_posts() ) :

			$counter = 1;

			echo '<div id="blog-container">';

			while( have_posts() ) : the_post();

				get_template_part('template-parts/blog/summary-loop'); 
				$counter++;

			endwhile;

			echo '</div>';

			get_template_part('template-parts/blog/pagination'); 

		else : ?>

		<div class="wrap">

			<h3><?php _e('Unfortunately!', 'teamnijhuis'); ?></h3>

			<p><?php _e('No items found.', 'teamnijhuis'); ?></p>
			
		</div>

	<?php endif; ?>


</div>
<!-- End #blog-page -->