<?php
	$categories 	= get_the_category( $post->ID);
	$category 		= $categories[0];
?>

<!-- Start #blog-content -->
<div id="blog-content" class="content-row">

	<div class="wrap">
	
		<div class="row">

			<div class="col-xs-12 col-lg-7">

				<p class="publish-date bigger"><?php echo ucfirst( get_the_date( 'F d, Y' ) ); ?></p>
				
				<h1><?php the_title(); ?></h1>

				<p class="publish-info"><?php printf( __('By %s | %s', 'teamnijhuis'), get_the_author_meta('display_name'), '<a href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '">' . $category->name . '</a>' ); ?></p>

				<?php the_content(); ?>

				<p class="back">
					<a href="<?php echo get_post_type_archive_link('post'); ?>" title="<?php esc_attr_e('Back to summary', 'teamnijhuis'); ?>"><i class="fa fa-long-arrow-alt-left"></i> <?php _e('Back to summary', 'teamnijhuis'); ?></a>
				</p>

			</div>

			<div class="col-xs-12 col-lg-4 col-lg-offset-1">

				<div class="block light-blue-bg white-text">
					
					<p class="h3"><?php _e('Share now!', 'teamnijhuis'); ?></p>

					<?php echo do_shortcode('[socialmedia-share-buttons]'); ?>
					
				</div>

				<div class="block white-block with-shadow">
					
					<p class="h3"><?php _e('Recent articles', 'teamnijhuis'); ?></p>

					<?php 
						$args = array(
							'post_type' 		=> 'post',
							'post_status' 		=> 'publish',
							'posts_per_page' 	=> 5,
							'post__not_in'		=> array(get_the_id())
						);
						$the_query = new WP_Query( $args );

						if ( $the_query->have_posts() ) : 
					?>

						<ul class="recent-articles">

							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								
								<li><a href="<?php the_permalink(); ?>" title="<?php esc_attr_e( get_the_title() ); ?>"><?php the_title(); ?></a></li>

							<?php endwhile; ?>

						</ul>

					<?php wp_reset_postdata(); endif; ?>

				</div>
				
			</div>

		</div>

	</div>

</div>
<!-- End #blog-content -->