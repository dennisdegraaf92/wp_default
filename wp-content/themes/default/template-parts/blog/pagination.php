<?php 
	global $wp_query;
	$big = 999999999;
	$pagination = '<nav class="pagination content-row light-grey-bg"><div class="wrap align-center">';
	$pagination .= paginate_links( array(
		'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format'		=> '/page/%#%/',
		'current'		=> max( 1, get_query_var('paged') ),
		'total'			=> $wp_query->max_num_pages,
		'prev_text'     => '<i class="fa fa-angle-left"></i>',
		'next_text'     => '<i class="fa fa-angle-right"></i>'						
	) );
	$pagination = str_replace('<a class="next', '<a rel="next" class="next ', $pagination);
	$pagination = str_replace('<a class="prev', '<a rel="prev" class="prev ', $pagination);
	$pagination .= '</div></nav>';
	echo $pagination;
?>