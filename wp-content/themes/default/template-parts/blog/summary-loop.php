<?php
	$categories 	= get_the_category( $post->ID);
	$category 		= $categories[0];
?>


<article class="news-summary">

	<div class="row reverse middle-xs">

		<div class="col-xs-12 col-lg-6 img">

			<p>
				<?php the_post_thumbnail('large', array('class' => 'lazy') ); ?>
			</p>

		</div>

		<div class="col-xs-12 col-lg-6">

			<div class="text-container white-text">
				
				<h2><?php the_title(); ?></h2>

				<?php the_excerpt(); ?>

				<p class="button-p">
					<a class="button ghost white" href="<?php the_permalink(); ?>" title="<?php esc_attr_e( get_the_title() ); ?>"><?php _e('Read more', 'teamnijhuis'); ?></a>
				</p>

			</div>

		</div>

	</div>

</article>

