<div class="content-row">

	<div class="wrap clearfix">

		<h1><?php printf( __('Searched for %s', 'teamnijhuis'), '&quot;' . get_search_query() . '&quot;' ); ?></h1>	

		<p><?php printf( __('Below you will find the results for your search term %s.', 'teamnijhuis'), '&quot;' . get_search_query() . '&quot;'); ?></p>

		<?php	
			global $wp_query;
			$args = array( 'posts_per_page' => -1 );
			query_posts(
				array_merge(
					$args,
					$wp_query->query
				)
			);

			if ( have_posts() ) :

				echo '	<div id="search-results">
							<ol>';

				while( have_posts() ) : the_post(); 

					echo '		<li>
									<a href="'. get_the_permalink() . '" title="'. esc_attr( get_the_title() ) .'">
										' . get_the_title() . '
									</a>
								</li>';

				endwhile;

				echo '		</ol>
						</div>';

			else :

				echo '<h3>' . __('Unfortunately!', 'teamnijhuis') . '</h3>';
				echo '<p>' . __('No matching results were found. Please try again.', 'teamnijhuis') . '</p>';

			endif;	

			wp_reset_postdata();
			wp_reset_query();
		?>

	</div>

</div>