<!-- Start #footer-nav -->
<div id="footer-nav" class="content-row">
	
	<div class="wrap">
		
		<div class="row">

			<?php 
				dynamic_sidebar('footer_first_block');

				dynamic_sidebar('footer_second_block');

				dynamic_sidebar('footer_third_block');

				dynamic_sidebar('footer_fourth_block'); 
			?>				

		</div>

	</div>

</div>
<!-- End #footer-nav -->