<div id="page-404" class="content-row">

	<div class="wrap">

		<h1><?php _e('Page not found (404)', 'teamnijhuis'); ?></h1>

		<p><?php printf( __('The page you requested is not available at this time. Click <a href="%s">here</a> to return to the homepage.', 'teamnijhuis'), get_site_url() ); ?></p>

	</div>

</div>