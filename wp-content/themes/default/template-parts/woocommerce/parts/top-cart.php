<div class="top-cart">
	<a href="<?php echo wc_get_page_permalink('cart') ?>" title="<?php esc_attr_e( 'Cart', 'teamnijhuis' ); ?>">
		<i class="fa fa-shopping-cart"></i>
	</a>
</div>