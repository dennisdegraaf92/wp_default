<!-- Start #main-nav -->
<nav id="main-nav">

	<div class="wrap">

		<?php 
			wp_nav_menu( array(
				'theme_location' 	=> 'main-navigation',
				'container_class'	=> 'main-nav-wrap'
			));

		?>

	</div>

</nav>
<!-- End #main-nav -->