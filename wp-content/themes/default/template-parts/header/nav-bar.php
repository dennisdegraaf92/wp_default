<!-- Start #nav-bar -->
<div id="nav-bar">

	<div class="wrap clearfix">

		<?php get_template_part('template-parts/global/site-logo'); ?>

		<?php 
			wp_nav_menu( array(
				'theme_location' 	=> 'main-navigation',
				'container_class'	=> 'main-nav-wrap'
			));
		?>

		<?php get_template_part('template-parts/global/mob-nav-button'); ?>

	</div>
	<!-- End .wrap -->

</div>
<!-- End #nav-bar -->