<button id="show-mob-nav" class="clearfix" onclick="toggleMobNav(this);" title="<?php esc_attr_e('Show navigation', 'teamnijhuis'); ?>">
	<span class="text"><?php _e('Menu', 'teamnijhuis'); ?></span>
	<span class="strokes">
		<span></span>
		<span></span>
		<span></span>
	</span>	
</button>