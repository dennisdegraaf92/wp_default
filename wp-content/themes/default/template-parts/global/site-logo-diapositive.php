<?php if ( get_field('logo_diapositief', 'option') ) : ?>

	<a class="site-logo" href="<?php echo site_url(); ?>" title="<?php esc_attr_e( get_bloginfo('name') ); ?>">

		<?php
			$img_id = get_field('logo_diapositief', 'option');
			if ( isset( $img_id ) && ( ! empty( $img_id ) ) )
				echo wp_get_attachment_image( $img_id, 'medium', false, array( 'class' => 'lazy' ) );
		?>

	</a>

<?php endif; ?>