<?php if( function_exists('yoast_breadcrumb') ) { ?>
				
	<div id="breadcrumbs">

		<div class="wrap">

			<?php yoast_breadcrumb( '<p class="breadcumb-text smaller">','</p>' ); ?>

		</div>

	</div> 

<?php } ?>	