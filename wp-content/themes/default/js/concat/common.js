var $j = jQuery.noConflict();

$j( document ).ready( function($) {

	$j('#mobile-navigation ul li.menu-item-has-children > a').each(function(){

		$(this).append('<span class="expand"><i class="fa fa-angle-down"></i></span>');

	});

	$j('#mobile-navigation ul li.menu-item-has-children a span.expand').click(function(){

		$(this).parent().next().slideToggle(300);
		$(this).toggleClass('active');
		return false;

	});

	$j('#mobile-navigation ul li.current-menu-ancestor a span.expand').each( function(){

		$(this).parent().next().slideToggle(300);
		$(this).addClass('active');
		return false;

	});

	if( window.location.hash !== ''  ) {

		var hash = window.location.hash;

		if( hash.length > 1 && $j(hash).length > 0 )
			scrollTo(hash);
	}

	$j('.anchor-link').click(function() {

		var target 			= $j(this).attr('href'); 
		scrollToElement(target);		
	});

	$j(window).scroll(function() {
		executeScrollAnimations();
	});

	executeScrollAnimations();

});


// Add lazyload JS functionality
var myLazyLoad = new LazyLoad({
	elements_selector: ".lazy"
});

// Toggle mobile navigation
function toggleMobNav( button ) {
	$j(this).toggleClass('active');
	$j('#mobile-navigation').slideToggle('fast');
}

// Functions to execute by scrollv
function executeScrollAnimations() {

	var scrollPosition = $j(window).scrollTop();

	if( scrollPosition > 0 ) {
		$j('body').addClass('scrolled');
	} else {
		$j('body').removeClass('scrolled');
	}

}

// Scroll to function
function scrollToElement(target) {

	var headerHeight	= $j('#main-header').outerHeight();
	var newPos 			= 0;

	if( target === parseInt( target, 10 ) ) {
		newPos = target;
	} else {
		newPos = jQuery(target).offset().top;
	}

	newPos = newPos - headerHeight;
	$j("html, body").animate({ scrollTop: newPos }, 1000);

	return false;

}